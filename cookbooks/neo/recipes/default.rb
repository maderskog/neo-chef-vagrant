version = node['neo']['version']
tarFile = "/tmp/neo4j-enterprise-#{version}-unix.tar.gz"
installLocation = "/opt/neo4j/"

directory installLocation do
  owner 'vagrant'
  group 'vagrant'
  action :create
end

remote_file tarFile do
       source "http://dist.neo4j.org/neo4j-enterprise-#{version}-unix.tar.gz"
       owner 'vagrant'
       group 'vagrant'
end

bash 'extract neo' do
    user 'vagrant'
    cwd '/tmp/'
    code <<-EOH
        tar xvf #{tarFile}
        mv neo4j-enterprise-#{version}/* #{installLocation}
    EOH
end

template "/opt/neo4j/conf/neo4j-server.properties" do
    source "neo4j-server.erb"
    group 'vagrant'
    user 'vagrant'
end

template "/opt/neo4j/conf/neo4j-wrapper.properties" do
    source "neo4j-wrapper.erb"
    group 'vagrant'
    user 'vagrant'
    variables( :port => node['neo']['debugPort'])
end

case version[0..0]
when "1"
    bash 'install neo 1.x.x' do
    	user 'root'
    	cwd "#{installLocation}/bin"
    	code <<-EOH
    		echo "vagrant" | ./neo4j install
    		service neo4j-service start
    	EOH
    end
when "2"
	bash 'install neo 2.x.x' do
    	user 'root'
    	cwd "#{installLocation}/bin"
    	code <<-EOH
    		./neo4j-installer install

    		vagrant
    		service neo4j-service start
    	EOH
    end
end
